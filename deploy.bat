@echo off
:: variables
set CONTAINER=kwetter-auth-service
set /p DOCKER_PASSWORD=<docker_password.txt

git describe --always --abbrev --tags --long > result.txt
set /p GIT_VERSION=<result.txt

:: use CALL when running another script file from a script file
CALL mvn clean install -DskipTests
CALL ./build_image.bat %CONTAINER% %DOCKER_PASSWORD% %GIT_VERSION%
CALL ./cleanup.bat %CONTAINER%
