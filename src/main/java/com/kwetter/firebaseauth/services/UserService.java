package com.kwetter.firebaseauth.services;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.kwetter.firebaseauth.objects.models.User;
import com.kwetter.firebaseauth.objects.models.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class UserService {

    @Autowired
    private Firestore dbFirestore;

    public static final String COLLECTION_NAME="users";

    public String saveUserDetails(UserData userData) throws InterruptedException, ExecutionException {
        User user = new User(userData);
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COLLECTION_NAME).document(user.getLocalId()).set(user);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public User getUser(String id) throws InterruptedException, ExecutionException {
        User user = null;

        // check if it's the test user, as to avoid overusage of max firebase request quota
        if(id.equals("vfMxJcul4oZjbVE8rajQ7ebnDiF3")){
            user = new User();
            user.LoadTestUser(id);
            return user;
        }

        DocumentReference documentReference = dbFirestore.collection(COLLECTION_NAME).document(id);
        ApiFuture<DocumentSnapshot> future = documentReference.get();

        DocumentSnapshot document = future.get();

        if(document.exists()) {
            user = document.toObject(User.class);
            return user;
        }else {
            return null;
        }
    }

    public String updateUserDetails(UserData userData) throws InterruptedException, ExecutionException {
        User user = new User(userData);
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COLLECTION_NAME).document(user.getLocalId()).set(user);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public String deleteUser(String id) {
        ApiFuture<WriteResult> writeResult = dbFirestore.collection(COLLECTION_NAME).document(id).delete();
        return "Document with User ID "+id+" has been deleted";
    }
}
