package com.kwetter.firebaseauth.services;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.kwetter.firebaseauth.helpers.tools.Helper;
import com.kwetter.firebaseauth.objects.models.UserData;
import com.kwetter.firebaseauth.objects.models.UserDetails;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserAccountService {

    private final RestTemplate restTemplate;
    private final HttpService httpService;

    public UserAccountService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        httpService = new HttpService(this.restTemplate);
    }

    public UserData createNewUser(UserDetails userDetails) {
        String url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBJCIgQcbKkrY5YplyI9beM_y0QyzgOE2k";

        // create a map for post parameters
        Map<String, Object> map = new HashMap<>();
        map.put("email", userDetails.getEmail());
        map.put("password", userDetails.getPassword());
        map.put("displayName", userDetails.getDisplayName());
        map.put("returnSecureToken", true);

        UserData userData = httpService.CreatePost(url, map);
        System.out.println("Successfully created new user: " + userData.getLocalId());

        return userData;
    }

    public String  UpdateUser(UserDetails userDetails) throws FirebaseAuthException {
        UserRecord.UpdateRequest request = new UserRecord.UpdateRequest(userDetails.getId());

        if(!Helper.IsEmpty(userDetails.getEmail())){
            request.setEmail(userDetails.getEmail());
        }

        if(!Helper.IsEmpty(userDetails.getDisplayName())){
            request.setDisplayName(userDetails.getDisplayName());
        }

//        if(!Helper.IsEmpty(userDetails.getPhotoUrl())){
//            request.setPhotoUrl(userDetails.getPhotoUrl());
//        }

        request.setDisabled(userDetails.isDisabled());

        UserRecord userRecord = FirebaseAuth.getInstance().updateUser(request);

        String message = "Successfully updated user: " + userRecord.getDisplayName();

        return message;
    }

    public String DeleteUser(String uid) throws FirebaseAuthException {
        FirebaseAuth.getInstance().deleteUser(uid);

        String message = "Successfully deleted user.";

        return message;
    }

    public String UpdateUserPassword(UserDetails userDetails) throws FirebaseAuthException {
        if(Helper.IsEmpty(userDetails.getPassword())){
            throw new IllegalArgumentException("The supplied password does not meet the requirements");
        }

        UserRecord.UpdateRequest request = new UserRecord.UpdateRequest(userDetails.getId())
                .setPassword(userDetails.getPassword());

        UserRecord userRecord = FirebaseAuth.getInstance().updateUser(request);

        String message = "Successfully updated password for user: " + userRecord.getDisplayName();

        return message;
    }
}