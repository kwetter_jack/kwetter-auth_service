package com.kwetter.firebaseauth.services;

import com.kwetter.firebaseauth.objects.models.UserData;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;

public class HttpService {

    private final RestTemplate restTemplate;

    public HttpService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public UserData CreatePost(String url, Map<String, Object> map) {
        try {
            // create headers
            HttpHeaders headers = new HttpHeaders();
            // set `content-type` header
            headers.setContentType(MediaType.APPLICATION_JSON);
            // set `accept` header
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            // build the request
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

            // send POST request
            ResponseEntity<UserData> response = this.restTemplate.postForEntity(url, entity, UserData.class);

            // check response status code
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            } else {
                return null;
            }
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            return null;
        }
    }
}
