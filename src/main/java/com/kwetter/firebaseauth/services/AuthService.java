package com.kwetter.firebaseauth.services;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.kwetter.firebaseauth.objects.models.LoginInfo;
import com.kwetter.firebaseauth.objects.models.Session;
import com.kwetter.firebaseauth.objects.models.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

//https://firebase.google.com/docs/reference/rest/auth

@Service
public class AuthService {

    @Autowired
    private Firestore dbFirestore;

    public static final String COLLECTION_NAME="session";

    private final RestTemplate restTemplate;
    private final HttpService httpService;

    public AuthService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        httpService = new HttpService(this.restTemplate);
    }

    public UserData LoginWithCredentials(LoginInfo loginRequest){
        String url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBJCIgQcbKkrY5YplyI9beM_y0QyzgOE2k";

        // create a map for post parameters
        Map<String, Object> map = new HashMap<>();
        map.put("email", loginRequest.getEmail());
        map.put("password", loginRequest.getPassword());
        map.put("returnSecureToken", loginRequest.getReturnSecureToken());

        UserData response = httpService.CreatePost(url, map);

        return response;
    }

    public void StoreLoginData(UserData userData, String ipAddress){
        Session sessionData = new Session();
        sessionData.LoadWithUserData(userData, ipAddress);

        dbFirestore.collection(COLLECTION_NAME).document(sessionData.getUid()).set(sessionData);
    }

    public String CreateAuthenticationToken(String uid) throws ExecutionException, InterruptedException {
        Map<String, Object> hash = new HashMap<>();

        String customToken = FirebaseAuth.getInstance().createCustomTokenAsync(uid).get();

        return customToken;
    }

    public String VerifyToken(String idToken) throws FirebaseAuthException {
        // idToken comes from the client app (shown above)
        FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
        String uid = decodedToken.getUid();

        return uid;
    }

}
