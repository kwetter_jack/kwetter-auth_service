package com.kwetter.firebaseauth.helpers.components;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.kwetter.firebaseauth.objects.DTO.RabbitDTO;
import com.kwetter.firebaseauth.objects.DTO.WriterDTO;
import com.kwetter.firebaseauth.objects.enums.MessageType;
import com.kwetter.firebaseauth.objects.models.User;
import com.kwetter.firebaseauth.services.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.kwetter.firebaseauth.helpers.tools.Helper.emptyIfNull;

@Component
public class UserComponent {

    @Autowired
    UserService userService;

    @RabbitListener(queues = "${jsa.rabbitmq.request.queue}")
    public String UserRequest(RabbitDTO rabbitDTO) throws InterruptedException {
        System.out.println("test");
        System.out.println("received requests: " + rabbitDTO);

        if(rabbitDTO.getType() == MessageType.SINGLE){
            ObjectMapper mapper = new ObjectMapper();
            WriterDTO writerDTO = mapper.convertValue(rabbitDTO.getContent(), WriterDTO.class);

            WriterDTO writer = getUser(writerDTO.getId());
            rabbitDTO.setContent(writer);
        }else{
            ArrayList<String> writer_ids = (ArrayList<String>) rabbitDTO.getContent();

            List<WriterDTO> writers = new ArrayList<>();
            for (String writer_id : emptyIfNull(writer_ids) ){
                WriterDTO writer = getUser(writer_id);
                writers.add(writer);
            }

            rabbitDTO.setContent(writers);
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(rabbitDTO);

        System.out.println("send response: " + jsonResponse);
        return jsonResponse;
    }

    private WriterDTO getUser(String id){
        User user = null;

        try{
            user = userService.getUser(id);

            WriterDTO writter = new WriterDTO(user);

            if(user == null || user.getLocalId() == null){
                System.out.println("no user found with id: " + writter.getId());
                return null;
            }

            return writter;
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
