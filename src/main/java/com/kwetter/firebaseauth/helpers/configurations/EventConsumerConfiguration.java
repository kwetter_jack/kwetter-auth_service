package com.kwetter.firebaseauth.helpers.configurations;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventConsumerConfiguration {

    @Value("${jsa.rabbitmq.request.queue}")
    private String requestQueue;

    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    @Bean
    public TopicExchange eventExchange() {
        return new TopicExchange(exchange);
    }

    @Bean
    public Queue requestQueue() {
        return new Queue(requestQueue);
    }

    @Bean
    public Binding binding(Queue requestQueue, TopicExchange eventExchange) {
        return BindingBuilder.bind(requestQueue).to(eventExchange).with("user.*");
    }

    @Bean
    public MessageConverter converter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory){
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}

