package com.kwetter.firebaseauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class FirebaseAuthApplication {

	public static void main(String[] args){
		SpringApplication.run(FirebaseAuthApplication.class, args);
	}

}
