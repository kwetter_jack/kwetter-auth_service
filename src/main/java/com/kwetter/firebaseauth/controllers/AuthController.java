package com.kwetter.firebaseauth.controllers;

import com.google.firebase.auth.FirebaseAuthException;
import com.kwetter.firebaseauth.objects.models.LoginInfo;
import com.kwetter.firebaseauth.objects.models.UserData;
import com.kwetter.firebaseauth.services.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@Controller
@Slf4j
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping(path="/validate/{token}")
    public ResponseEntity<UserData> verifySessionCookie(HttpServletRequest request, @PathVariable String token) {
        // get client ip address
        String ip = request.getRemoteAddr();

        try {
            String uid = authService.VerifyToken(token);
            UserData userData = new UserData(uid, token);

            System.out.println("authentication successful for user: " + uid);

            return new ResponseEntity<>(userData, HttpStatus.OK);
        } catch (FirebaseAuthException e) {
            e.printStackTrace();

            return new ResponseEntity<>(new UserData(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(path="/login")
    public ResponseEntity<UserData> loginWithCredentials(HttpServletRequest request, @RequestBody LoginInfo loginRequest) {
        UserData response = authService.LoginWithCredentials(loginRequest);

        if(response == null){
            String message = "No user could be found with those credentials";
            System.out.println(message);
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }

        // get client ip address
        String ip = request.getRemoteAddr();

        authService.StoreLoginData(response, ip);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
