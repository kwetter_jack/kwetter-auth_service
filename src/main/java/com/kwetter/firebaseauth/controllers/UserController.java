package com.kwetter.firebaseauth.controllers;

import com.google.firebase.auth.FirebaseAuthException;
import com.kwetter.firebaseauth.objects.models.User;
import com.kwetter.firebaseauth.objects.models.UserData;
import com.kwetter.firebaseauth.objects.models.UserDetails;
import com.kwetter.firebaseauth.services.AuthService;
import com.kwetter.firebaseauth.services.UserAccountService;
import com.kwetter.firebaseauth.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

@CrossOrigin
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserAccountService userManagementService;

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @GetMapping(path = "/id/{uid}")
    public ResponseEntity<User>  getUser(HttpServletRequest request, @PathVariable String uid) throws ExecutionException, InterruptedException {
        User user = userService.getUser(uid);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping(path="/register")
    public ResponseEntity<UserData> RegisterUser(HttpServletRequest request, @RequestBody UserDetails userDetails) throws ExecutionException, InterruptedException {
        UserData userData = userManagementService.createNewUser(userDetails);

        // get client ip address
        String ip = request.getRemoteAddr();

        authService.StoreLoginData(userData, ip);
        userService.saveUserDetails(userData);

        return new ResponseEntity<>(userData, HttpStatus.OK);
    }

    @PostMapping(path ="/account/update")
    public ResponseEntity<String> UpdateUserAccount(HttpServletRequest request, @RequestBody UserDetails userDetails) {
        try{

            String message = userManagementService.UpdateUser(userDetails);
            System.out.println(message);

            return new ResponseEntity<>(message, HttpStatus.OK);

        }catch (FirebaseAuthException e){
            e.printStackTrace();

            var message = e.getMessage();
            if( e.getAuthErrorCode().name() == "EMAIL_ALREADY_EXISTS"){
                message = "User with supplied email already exists";
            }

            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path ="/changePassword")
    public ResponseEntity UpdateUserPassword(@RequestBody UserDetails userDetails) {
        try{

            String message = userManagementService.UpdateUserPassword(userDetails);
            System.out.println(message);

            return new ResponseEntity<>(message, HttpStatus.OK);

        }catch (FirebaseAuthException | IllegalArgumentException e){
            e.printStackTrace();
            var message = e.getMessage();

            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(path = "/delete/{uid}")
    public ResponseEntity DeleteUser(@PathVariable String uid) {
        try{
            String message = userManagementService.DeleteUser(uid);
            System.out.println(message);

            return new ResponseEntity<>(message, HttpStatus.OK);
        } catch (FirebaseAuthException e) {
            e.printStackTrace();

            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
