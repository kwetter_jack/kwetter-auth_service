package com.kwetter.firebaseauth.objects.enums;

public enum Permission {
    READ,
    WRITE
}
