package com.kwetter.firebaseauth.objects.enums;

public enum MessageType {
    SINGLE,
    LIST
}
