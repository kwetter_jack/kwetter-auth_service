package com.kwetter.firebaseauth.objects.enums;

public enum Role {
    Administrator,
    Moderator,
    User
}
