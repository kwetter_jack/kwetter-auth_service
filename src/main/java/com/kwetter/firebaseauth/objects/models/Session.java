package com.kwetter.firebaseauth.objects.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Session {
    private String uid;
    private String displayName;
    private String ipAddress;
    private String Token;
    private String role;
    private Boolean active;

    public Session() {
    }

    public void LoadWithUserData(UserData userData, String ipAddress){
        this.uid = userData.getLocalId();
        this.displayName = userData.getDisplayName();
        this.ipAddress = ipAddress;
        this.Token = userData.getIdToken();
        this.role = userData.getRole().toString();
        this.active = true;
    }
}
