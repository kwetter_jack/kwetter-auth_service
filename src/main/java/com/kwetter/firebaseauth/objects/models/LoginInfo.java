package com.kwetter.firebaseauth.objects.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginInfo {
    private String email;
    private String password;
    private Boolean returnSecureToken = true;

    public LoginInfo() {
    }

    public LoginInfo(String email, String password, Boolean returnSecureToken) {
        this.email = email;
        this.password = password;
        this.returnSecureToken = returnSecureToken;
    }
}
