package com.kwetter.firebaseauth.objects.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetails {
    private String id;
    private String email;
    private String password;
//    private String phoneNumber;
    private boolean emailVerified = false;
    private String displayName;
//    private String photoUrl;
    private boolean disabled = false;

    public UserDetails() {
    }

}
