package com.kwetter.firebaseauth.objects.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
    private String localId;
    private Boolean active = true;
    private String email;
    private String displayName;

    public User(UserData userData){
        this.localId = userData.getLocalId();
        this.email = userData.getEmail();
        this.displayName = userData.getDisplayName();
    }

    public void LoadTestUser(String id){
        this.localId = id;
        this.email = "test@test.com";
        this.displayName = "test";
    }
}
