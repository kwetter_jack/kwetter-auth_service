package com.kwetter.firebaseauth.objects.models;

import com.google.firebase.auth.UserRecord;
import com.kwetter.firebaseauth.objects.enums.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserData {
    private String localId;
    private String email;
    private String displayName;
    private String idToken;
    private Role role = Role.User;

    public UserData() {
    }

    public UserData(UserRecord userRecord, String token){
        this.localId = userRecord.getUid();
        this.email = userRecord.getEmail();
        this.displayName = userRecord.getDisplayName();
        this.idToken = token;
    }

    public UserData(String uid, String token) {
        this.localId = uid;
        this.idToken = token;
    }
}
