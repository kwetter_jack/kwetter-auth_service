package com.kwetter.firebaseauth.objects.DTO;

import com.kwetter.firebaseauth.objects.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RabbitDTO {
    private Object content;
    private MessageType type;
}
