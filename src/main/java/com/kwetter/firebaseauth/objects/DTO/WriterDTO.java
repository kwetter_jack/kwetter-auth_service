package com.kwetter.firebaseauth.objects.DTO;

import com.kwetter.firebaseauth.objects.models.User;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class WriterDTO {
    private String id;
    private String displayName;

    public WriterDTO(User user){
        this.id = user.getLocalId();
        this.displayName = user.getDisplayName();
    }
}
